let brojZaPogađanje = Math.ceil(Math.random() * 20);
const slika = document.querySelector(".pogodak")

document.querySelector(".proveri").addEventListener("click", function () {
    let inputBroj = document.querySelector(".upisanBroj").value;
    if (inputBroj < 1 || inputBroj > 20) {
        document.querySelector(".rešenje").innerHTML = "Broj treba da bude od 1 do 20";
    }
    else if (inputBroj < brojZaPogađanje) {
        document.querySelector(".rešenje").innerHTML = "Uneti boj je manji od zadatog";
    }
    else if (inputBroj > brojZaPogađanje) {
        document.querySelector(".rešenje").innerHTML = "Uneti boj je veći od zadatog";
    }
    else if (inputBroj == brojZaPogađanje) {
        document.querySelector(".rešenje").innerHTML = "Čestitam, pogodili ste broj!";
        slika.src = "Smile.png"
    }
})

document.querySelector(".restart").addEventListener("click", function () {
    document.location.reload(true);
})