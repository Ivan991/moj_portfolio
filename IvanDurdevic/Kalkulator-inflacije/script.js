function inflationCalculator() {
    let stopaInflacije = Number(document.querySelector("#inflationRate").value);
    let novac = Number(document.querySelector("#money").value);
    let godine = Number(document.querySelector("#years").value);
    let vrednost = novac + (novac * (stopaInflacije / 100));

    for (let i = 1; i < godine; i++) {
        vrednost += vrednost * (stopaInflacije / 100);
    }
    vrednost = vrednost.toFixed(2)
    document.querySelector(".answer").innerHTML = `Za ${godine} godina ${novac} evra po stopi inflacije
    od ${stopaInflacije}% će vredeti ${vrednost} evra.`
}

document.querySelector(".restart").addEventListener("click", function () {
    document.location.reload(true);
})