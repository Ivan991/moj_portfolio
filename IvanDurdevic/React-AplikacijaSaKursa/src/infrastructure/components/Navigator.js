import React from 'react';
// import React, { useContext } from "react";
import { useNavigate } from 'react-router-dom';
import './Navigator.css';

import LanguageDropdown from './LanguageDropdown';
import { LocalizationContext } from '../../context/LanguageContext';

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import Container from '@mui/material/Container';
import Stack from "@mui/material/Stack";
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import Button from '@mui/material/Button';
import { IconButton } from '@mui/material';

const Navigator = () => {
    const { t } = React.useContext(LocalizationContext);
    const navigate = useNavigate()

    const logout = () => async () => {
        localStorage.removeItem('token');
        localStorage.removeItem('refreshToken');
        navigate('/');
        navigate(0)

    };

    return (
        <Box>
            <AppBar className="app-bar-2" position="static" sx={{ bgcolor: 'error.main'}}>
                <Container maxWidth="xl">
                    <Toolbar disableGutters height="max-content" className="toolbar-2">
                        <Box>
                            <Link 
                            className="logo" 
                            href="/"
                            component="a"
                            sx={{
                                mr: 1,
                                display: { xs: 'none', md: 'flex' },
                            }}
                            >
                            {/* <img src="friendly_logo.png" alt="logo" /> */}
                            <img src="https://imgur.com/zUhfWXf.png" alt="logo" to="/" />
                            </Link>
                        </Box>
                
                        <Box 
                            sx={{
                                flexGrow: 0,
                                marginLeft: 'auto',
                                '&:hover': {
                                    cursor: 'pointer',
                                    color: '#000045'
                                }
                            }}>
                            <Link onClick={logout()} color='inherit' underline='none'>{t('logout')}</Link>
                        </Box>

                        <Stack direction={"row"} spacing={2} className='language-dropdown'>
                            <LanguageDropdown />
                        </Stack>

                    </Toolbar>

                    <Box
                        /* variant='h6' */
                        noWrap
                        component='div'
                        sx={{ 
                            mr: 2, 
                            display: { xs: 'none', md: 'flex' },
                            color: 'error',
                            "&:hover": {
                                backgroundColor: "transparent",
                                 cursor: "default"
                            }
                        }}
                        disableRipple={true}
                    >
                        <Button href='/profile' variant="contained" className='profile-avatar' color="error" endIcon={<AccountCircleIcon />}
                            sx={{ 
                                mr: 2, 
                                display: { xs: 'none', md: 'flex' }, 
                            }}
                        >
                            {t('profile')}
                        </Button>
                    </Box>
                </Container>    
            </AppBar>
        </Box>
    );
};

export default Navigator;
