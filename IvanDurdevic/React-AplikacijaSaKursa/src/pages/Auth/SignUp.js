import React from 'react';
import {useNavigate} from 'react-router-dom'
import { LocalizationContext } from '../../context/LanguageContext';
import './SignUp.css';
import NavigatorAuth from "./../../infrastructure/components/NavigatorAuth";

import * as yup from 'yup';
import { useFormik } from 'formik';

import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';

// Pasword Visibily Eye-icon ON/OFF

import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';

// Backend API

import client_auth from '../../apis/client_auth';


function Copyright(props) {
  const { t } = React.useContext(LocalizationContext);
  return (
    <Typography className="copyright" variant="body2" color="#ffff" align="center" {...props}>
      {t("copyright")}
      <Link color="inherit" href="https://mui.com/">
        {t("copyright2")}
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}


export default function SignUp() {
  const { t } = React.useContext(LocalizationContext);

  const navigate = useNavigate()

  const [values, setValues] = React.useState({
    name: '',
    email: '',
    username: '',
    password: '',
    re_password: '',
    showPassword: false,
    show_re_password: false,
  });

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleClickShowConfirmPassword = () => {
    setValues({
      ...values,
      showConfirmPassword: !values.showConfirmPassword,
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleMouseDownConfirmPassword = (event) => {
    event.preventDefault();
  };

const [error, setError] = React.useState('')

  const onSignUp = async (values) => {
    try {
      const newUser = {
        name: values.name,
        username: values.username,
        email: values.email,
        password: values.password,
        re_password: values.re_password,
      };


      const reg = await client_auth.post(`/auth/users/`, newUser)

      if (reg.status === 201) {
        alert('Uspesno ste se registrovali')
        navigate('/signin')
        navigate(0)
      }


    } catch (error) {
      let err = Object.values(error.response.data)

      if (err[0][0] === 'The password is too similar to the email address.') {
        setError('Lozinka ne sme biti slicna imejlu')
      }
      else if (err[0][0] === 'This password is too common.') {
        setError('Ova lozinka je uobicajena')
      }
      else if (err[0][0] === 'A user with that username already exists.') {
        setError('Ovo korisničko ime već postoji.')
      }
      else {
        setError('Greska')
      }
      return

    }
  }

  const validationSchema = yup.object({
    name: yup.string()
      .required('Obavezno polje'),
    username: yup.string()
      .required('Obavezno polje'),
    email: yup.string()
      .email('Neispravan imejl')
      .required('Obavezno polje'),
    password: yup.string()
      .required('Obavezno polje')
      .min(8, 'Minimum 8 karaktera')
      .max(40, 'Maksimum 40 karaktera'),
    re_password: yup.string()
      .required('Obavezno polje')
      .oneOf([yup.ref('password'), null], 'Lozinke se ne podudaraju'),
  });

  const formik = useFormik({
    initialValues: values,
    validationSchema: validationSchema,
    onSubmit: onSignUp,
  });

  return (
    <div className="background-custom">
      <div className="container">
        <NavigatorAuth />
        
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <Box
            sx={{
              marginTop: 3,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              bgcolor: '#ffff',
              padding: 2.5,
              boxShadow: 8,
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: 'error.main' }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              {t("signUp")}
            </Typography>
            <Box component="form" noValidate onSubmit={formik.handleSubmit} sx={{ mt: 3 }}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    size="small"
                    color="error"
                    autoComplete="name"
                    name="name"
                    required
                    fullWidth
                    id="name"
                    label={t('nameSurname')}
                    value={formik.values.name}
                    onChange={formik.handleChange}
                    error={formik.touched.name && Boolean(formik.errors.name)}
                    helperText={formik.touched.name && formik.errors.name}
                    autoFocus
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    size="small"
                    color="error"
                    required
                    fullWidth
                    id="username"
                    label={t("username")}
                    value={formik.values.username}
                    onChange={formik.handleChange}
                    error={formik.touched.username && Boolean(formik.errors.username)}
                    helperText={formik.touched.username && formik.errors.username}
                    name="username"
                    autoComplete="username"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    size="small"
                    color="error"
                    required
                    fullWidth
                    id="email"
                    label={t("emailAddress")}
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    error={formik.touched.email && Boolean(formik.errors.email)}
                    helperText={formik.touched.email && formik.errors.email}
                    name="email"
                    autoComplete="email"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    size="small"
                    color="error"
                    fullWidth
                    name="password"
                    label={t("password")}
                    type={values.showPassword ? 'text' : 'password'}
                    id="password"
                    autoComplete="current-password"
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    error={formik.touched.password && Boolean(formik.errors.password)}
                    helperText={formik.touched.password && formik.errors.password}
                    
                    InputProps={{
                      endAdornment: <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                        >
                          {values.showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    size="small"
                    color="error"
                    required
                    fullWidth
                    name="re_password"
                    label={t("confirmPassword")}
                    type={values.showConfirmPassword ? 'text' : 'password'}
                    value={formik.values.re_password}
                    onChange={formik.handleChange}
                    error={formik.touched.re_password && Boolean(formik.errors.re_password)}
                    helperText={formik.touched.re_password && formik.errors.re_password}
                    id="re_password"
                    autoComplete="new-password"

                    InputProps={{
                      endAdornment: <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowConfirmPassword}
                          onMouseDown={handleMouseDownConfirmPassword}
                          edge="end"
                        >
                          {values.showConfirmPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    }}
                  />
                </Grid>
                {/* <Grid item xs={12}>
                  <FormControlLabel
                    control={<Checkbox value="allowExtraEmails" color="error" />}
                    label={t("notifications")}
                  />
                </Grid> */}
              </Grid>
              <Button
                color="error"
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                {t("signUp")}
              </Button>
              {error !== '' && <p style={{color: 'red'}}>{error}</p>}
              <Grid container justifyContent="flex-end">
                <Grid item>
                  <Link href="/signin" variant="body2" color="error.main"
                  // onClick={navigateSignIn}
                  >
                    {t('questionAccountSU')}
                  </Link>
                </Grid>
              </Grid>
            </Box>
          </Box>
          <Copyright sx={{ mt: 1.75, mb: 10.25 }} />
        </Container>
      </div>
    </div>
  );
}