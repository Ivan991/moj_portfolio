# Uputstvo za korišćenje aplikacije

Ova aplikacija služi za praćenje karbonskog otisška. 

## Važna napomena!

Kalkulator za izračunavanje karbonskog otiska je funkcionalan jedino u Google Chrome pretraživaču uz instalaciju i pokretanje ekstenzije Allow CORS koja se nalazi na linku https://chrome.google.com/webstore/detail/allow-cors-access-control/lhobafahddgcelffkeicbaginigeejlf/related?hl=en.

## Korisnici

Ima dve vrste korisnika: administrator i registrovani korisnici. Sistemu mogu pristupiti i neregistrovani korisnici, ali je njima dostupna samo početna stranica za neregistrovane korisnike.

### Administrator

Njemu nije neophodna registracija, već se samo loguje na istoj stranici gde to rade i registrovani korisnici. Njegovo korisničko ime je Admin, a šifra Admin91$. Kada je ulogovan, na njegovoj početnoj strani je tabela sa verifikovanim registrovanim korisnicima. Korisnike u toj tabeli može da obriše svakog pojedinačno, nakon čega se obrisani član uklanja iz baze registrovanih korisnika, tj. briše se iz Local Storage-a. U okviru Navbar-a, administrator može odabrati opciju Korisnici koja ga vodi na stranicu u okviru koje se nalazi tabela sa listom registrovanih neverifikovanih korisnika. Pored svakog od njih se nalazi dugme za verifikaciju, nakion čijeg odabira odgovarajući korisnik postaje verifikovan, tj. može da se uloguje i pojavljuje se u tabeli administratorove početne stranice. Uz prethodno pomenuto, administrator može odabrati opciju Rang lista koja ga vodi na stranicu sa rang listom korisničkih karbonskih otisaka.

### Registrovani korisnici

Nakon što novi korisnik unese odgovarajuće podatke u registarsku formu, još uvek ne može da se uloguje dok ga administrator ne verifikuje. Nakon što bude verifikovn, korissnik nakon logovanja ima pristup sledećim stranicama: profilnoj strani koja prikazuje korisničke podatke unete prilikom registracije; u okviru stranice za aktivnosti, korisnik može izračunati svoj karbonski otisak neograničeno puta, a pojedinačni rezultati se sabiraju u ukupan zbir; početna strana za ulogovane korisnike. Takože, u okviru profilne strane, korisnik može otvoriti modal za promenu svoje lozinke. 

## Responzivnost

Stranice u okviru aplikacije su iskreirane da budu responzivne kako za računar, tako za tablet i mobilni telefon.