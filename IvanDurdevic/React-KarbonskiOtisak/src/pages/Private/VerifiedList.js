import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { Paper } from '@mui/material';
import { DataGrid } from '@mui/x-data-grid';
import Button from '@mui/material/Button';
import { Typography } from '@mui/material';
import Box from '@mui/material/Box';
import { styled } from '@mui/material/styles';
import { green } from '@mui/material/colors';
import NavBarAdmin from '../../components/NavBarAdmin';
import Link from '@mui/material/Link';


function Copyright(props) {
    return (
      <Typography className="copyright" variant="body2" color="#fff" align="center" {...props}>
        {'Copyright©'}
        <Link color="inherit" href="https://mui.com/">
           www.karbonskiotisak.com
        </Link>{' '}
        {new Date().getFullYear()}.
      </Typography>
    );
}

const ColorButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(green[500]),
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[700],
    },
  }));

export default function HomePageAdmin() {
    const [registerUsers, setRegisterUsers] = useState([]);
    const [registerUsersNotVerified, setRegisterUsersNotVerified] = useState([]);

    useEffect(() => {
        setRegisterUsers(JSON.parse(localStorage.users));
    }, [])

    useEffect(() => {
        const registerUsersNotVerified = registerUsers.filter(user => //Filter metoda vraca niz.
            user.verified === false
        );
        console.log(registerUsersNotVerified);
        setRegisterUsersNotVerified(registerUsersNotVerified);
    }, [registerUsers])

    const columns = [
        { field: 'name', headerName: 'Ime i prezime', width: 200 },
        { field: 'email', headerName: 'Mejl', width: 250 },
        {
            field: 'username',
            headerName: 'Korisničko ime',
            width: 160,
        },
        {
            field: "action", 
            headerName: "Action", 
            width: 180, 
            description: 'This column has a value getter and is not sortable.',
            sortable: false,
            
            renderCell: (params) => {
                const verifyUser = e => {
                    params.row.verified = true; //Kraci nacin pisanja... :)
                    e.stopPropagation();
                    // Tražimo korisnika sa korisničkim imenom kao u redu na koji je Admin kliknuo
                    registerUsers.forEach(user => {
                        if (user.username === params.row.username) {
                            user.verified = true;
                        }
                    });
                    // Osvežavamo podatke u nizu registerUserNotVerified tako što izbacimo korisnika sa usernameom na koga je Admin kliknuo i verifikovao
                    setRegisterUsersNotVerified(registerUsersNotVerified.filter(user => user.username !== params.row.username))
                    localStorage.setItem('users', JSON.stringify(registerUsers))
                }; return <ColorButton variant="contained" sx={{color: 'white', backgroundColor: 'green'}} onClick={verifyUser}>Verifikuj</ColorButton>;
            }
        }];


    return (
        <div>
            <NavBarAdmin />
            <Paper className="background-image-admin"
                sx={{
                    backgroundColor: 'greenyellow',
                    backgroundSize: 'cover',
                    backgroundPosition: 'center',
                    minHeight: '100vh',
                    maxWidth: `calc(98vw + 48px)`,
                    flexDirection: 'column', 
                    pt: 4
                }}
            >

                {/* DESKTOP VERZIJA - NASLOVA */}
                <Typography component="h1" variant="h5"
                    sx={{
                        fontSize: 26,
                        mt: 2,
                        mb: 5,
                        display: { xs: 'none', md: 'flex' },
                        /* textTransform: "uppercase",  */
                        alignText: "center",
                        alignContent: "center",
                        justifyContent: "center"
                    }}
                >
                    Lista korisnika za verifikaciju
                </Typography>

                {/* MOBILNA VERZIJA - NASLOVA */}
                <Typography /* component="h1" */ variant="h6"
                    sx={{
                        fontSize: 21,
                        mt: 2,
                        mb: 3,
                        display: { xs: 'flex', md: 'none' },
                        /* textTransform: "uppercase",  */
                        alignText: "center",
                        alignContent: "center",
                        justifyContent: "center"
                    }}
                >
                    Lista korisnika za verifikaciju
                </Typography>
                <Box sx={{ display: { xs: 'none', md: 'flex' }, height: 400, width: '60%', marginLeft: '20%', backgroundColor: 'ButtonFace' }}>
                    <DataGrid
                        rows={registerUsersNotVerified}
                        columns={columns}
                        getRowId={(row) => row.username}
                        pageSize={10}
                        rowsPerPageOptions={[10]}
                    // checkboxSelection
                    />
                </Box>
                <Box sx={{ display: { xs: 'flex', md: 'none' }, height: 400, width: '98%', marginLeft: '1%', backgroundColor: 'ButtonFace' }}>
                    <DataGrid
                        rows={registerUsersNotVerified}
                        columns={columns}
                        getRowId={(row) => row.username}
                        pageSize={10}
                        rowsPerPageOptions={[10]}
                    // checkboxSelection
                    />
                </Box>
                
                <Copyright className="copyright"
                    sx={{
                        position: 'absolute',
                        display: 'flex',
                        color: 'white',
                        mt: 7,
                        mb: 0.5,
                        right: '50%',
                        left: '50%',
                        alignContent: 'center',
                        justifyContent: 'center',
                        bottom: 0
                    }}
                />
            </Paper>

        </div>
    );
}
