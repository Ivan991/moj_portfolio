import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import '../HomePage.css';
import NavBarAdmin from '../../components/NavBarAdmin';
import { styled } from '@mui/material/styles';
import { green } from '@mui/material/colors';
import Img from '../Picture/Img.jpg';
import { Paper } from '@mui/material';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { DataGrid } from '@mui/x-data-grid';

const ColorButton = styled(Button)(({ theme }) => ({
  color: theme.palette.getContrastText(green[500]),
  backgroundColor: green[500],
  '&:hover': {
    backgroundColor: green[700],
  },
}));

const styles = {
  paperContainer: {
    height: 740,
    Width: 910,
    backgroundImage: `url(${Img})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    minHeight: '100vh',
    maxWidth: `calc(98vw + 48px)`,
    padding: 10
  }
};

function Copyright(props) {
  return (
    <Typography className="copyright" variant="body2" color="#fff" align="center" {...props}>
      {'Copyright_© '}
      <Link color="inherit" href="https://mui.com/">
        www.karbonskiotisak.com
      </Link>{' '}
      {new Date().getFullYear()}.
    </Typography>
  );
}

const HomePageAdmin = () => {
  const [registerUsers, setRegisterUsers] = useState([]);
  const [registerUsersVerified, setRegisterUsersVerified] = useState([]);

  useEffect(() => {
    setRegisterUsers(JSON.parse(localStorage.users));
  }, [])

  useEffect(() => {
    const registerUsersVerified = registerUsers.filter(user => 
      user.verified === true
    );
    console.log(registerUsersVerified);
    setRegisterUsersVerified(registerUsersVerified);
  }, [registerUsers])

  const columns = [
    { field: 'name', headerName: 'Ime i prezime', width: 200 },
    { field: 'email', headerName: 'Mejl', width: 250 },
    {
      field: 'username',
      headerName: 'Korisničko ime',
      width: 160,
    },
    {
      field: "action",
      headerName: "Action",
      width: 180,
      description: 'This column has a value getter and is not sortable.',
      sortable: false,

      renderCell: (params) => {
        const deleteUser = e => {
          e.stopPropagation();
          let filteredRegisterUsers = registerUsers.filter(user => user.username !== params.row.username)
          setRegisterUsers(filteredRegisterUsers)
          localStorage.setItem('users', JSON.stringify(filteredRegisterUsers))
        }; 
        return <ColorButton variant="contained" sx={{ color: 'white', backgroundColor: 'green' }} onClick={deleteUser}>Obriši</ColorButton>;
      }
    }];


  return (
    <div>
      <NavBarAdmin />

      <Paper className="background-image-home" style={styles.paperContainer}>
        <Box>
          <Box className="content" >

            <Typography className="heading" variant="h2" component="h2" mt={5} mr={4} style={{ marginLeft: 'auto' }} justifyContent="center"
              sx={{
                fontSize: {
                  lg: 50,
                  md: 50,
                  sm: 43,
                  xs: 36
                }
              }}
            >
              Lista korisnika:
            </Typography>
          </Box>
          <Box>
            {/* <Header onDrawerToggle={handleDrawerToggle} /> */}
            <Copyright className="copyright"
              sx={{
                display: 'flex',
                color: 'white',
                mt: 6.5,
                mb: 0.5,
                right: '50%',
                left: '50%',
                position: 'fixed',
                alignContent: 'center',
                justifyContent: 'center',
                bottom: 0
              }}
            />
          </Box>
          <Box sx={{ display: { xs: 'none', md: 'flex' }, height: 400, width: 900, backgroundColor: 'ButtonFace' }}>
            <DataGrid
              rows={registerUsersVerified}
              columns={columns}
              getRowId={(row) => row.username}
              pageSize={10}
              rowsPerPageOptions={[10]}
            // checkboxSelection
            />
          </Box>
        <Box sx={{ display: { xs: 'flex', md: 'none' }, height: 400, width: '98%', marginLeft: '1%', backgroundColor: 'ButtonFace' }}>
          <DataGrid
            rows={registerUsersVerified}
            columns={columns}
            getRowId={(row) => row.username}
            pageSize={10}
            rowsPerPageOptions={[10]}
          // checkboxSelection
          />
        </Box>
        </Box>
      </Paper>
    </div >
  )
}

export default HomePageAdmin;
